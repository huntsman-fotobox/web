///<reference path='typings/references.ts' />

class BaseWidget {

    protected config: any;
    protected api: API = new API();
    protected route: Route = new Route();
    protected storage: Store = new Store();
    protected options: any = null;

    constructor(config) {
        this.config = config;
        this.options = jQuery(this.config.container).data();
    }

    public init(): Q.Promise<boolean> {
        throw new Error('init() must be implemented!');
    }


    /**
     * get rendered Template
     *
     * @param templateName
     * @param data
     */
    public getTemplate(templateName: string, data?: any): Q.Promise<JQuery> {
        const deferred = Q.defer<JQuery>();
        const templateUrl = app_settings.base_url + '/templates/widgets/' + this.config.templatePath;

        BaseWidget.render(templateUrl, templateName, data).then((template) => {
            deferred.resolve(template);
        });

        return deferred.promise;
    }


    /**
     * render Template with Nunjucks
     * https://mozilla.github.io/nunjucks/templating.html
     *
     * @param templateUrl
     * @param templateName
     * @param context
     */
    private static render(templateUrl, templateName, context): Q.Promise<JQuery> {
        const deferred = Q.defer<JQuery>();

        let env = nunjucks.configure(templateUrl, {autoescape: true});
        new TemplateFilters().registerFilters(env);

        let template = nunjucks.render(templateName, context);

        deferred.resolve(template);
        return deferred.promise;
    }
}