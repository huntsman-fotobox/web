///<reference path='typings/references.ts' />

class BaseComponent {

    protected config: any;
    protected api: API = new API();
    protected route: Route = new Route();
    protected storage: Store = new Store();
    protected options: any = {};

    constructor(config) {
        this.config = config;
        this.options = jQuery(this.config.container).data();
    }

    public init(): Q.Promise<boolean> {
        throw new Error('init() must be implemented!');
    }


    /**
     * get rendered Template
     *
     * @param templateName
     * @param data
     */
    public getTemplate(templateName: string, data?: any): Q.Promise<JQuery> {
        const deferred = Q.defer<JQuery>();
        const templateUrl = app_settings.base_url + '/templates/components/' + this.config.templatePath;

        BaseComponent.render(templateUrl, templateName, data).then((template) => {
            deferred.resolve(template);
        });

        return deferred.promise;
    }


    /**
     * render Template with Nunjucks
     * https://mozilla.github.io/nunjucks/templating.html
     *
     * @param templateUrl
     * @param templateName
     * @param context
     */
    private static render(templateUrl, templateName, context): Q.Promise<JQuery> {
        const deferred = Q.defer<JQuery>();

        let env = nunjucks.configure(templateUrl, {autoescape: true});
        new TemplateFilters().registerFilters(env);

        let template = nunjucks.render(templateName, context);

        deferred.resolve(template);
        return deferred.promise;
    }


    /**
     * register nested vt-elements and start
     */
    public bind() {
        const vt = new VT3000();

        vt.fetchElementsFromDom(this.config.container);
    }


    /**
     *
     * @param componentName
     * @param selector
     * @param options
     */
    public installComponentAt(componentName: string, selector: string, options: any = {}): Q.Promise<boolean> {
        const defer = Q.defer<boolean>();
        const vt = new VT3000();
        let $elem = this.config.container.find(selector);

        _.forEach(options, (value, key) => {
            $elem.attr("data-" + key, value);
        });

        vt.registerElement('component', componentName, $elem);

        defer.resolve(true);
        return defer.promise;
    }


    /**
     *
     * @param widgetname
     * @param selector
     * @param options
     */
    public installWidgetAt(widgetname: string, selector: string, options: any = {}): Q.Promise<boolean> {
        const defer = Q.defer<boolean>();
        const vt = new VT3000();
        let $elem = this.config.container.find(selector);

        _.forEach(options, (value, key) => {
            $elem.attr("data-" + key, value);
        });

        vt.registerElement('widget', widgetname, $elem);

        defer.resolve(true);
        return defer.promise;
    }
}