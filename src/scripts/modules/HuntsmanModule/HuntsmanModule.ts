///<reference path='../../VT3000/typings/references.ts' />

class HuntsmanModule extends BaseModule {

    protected streamChannel: PubSub;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.streamChannel = PubSub.join('#Stream');

        this.registerPubSubHandler();
        this.getTemplate("view.html", {}).then((t: JQuery) => {
            this.config.container.append(t);
            this.bind();

            deferred.resolve(true);
        });

        return deferred.promise;
    }


    /**
     *
     */
    private registerPubSubHandler() {
        this.streamChannel.on('snapshot', (resp) => {
            this.saveSnapshot(resp); // send to server
            // this.localSaveSnapshot(resp.snap); // save locally
        });
    }


    /**
     * save snapshot by server
     * Try Example
     * https://ourcodeworld.com/articles/read/322/how-to-convert-a-base64-image-into-a-image-file-and-upload-it-with-an-asynchronous-form-using-jquery
     *
     * @param stream
     */
    private saveSnapshot(stream) {
        // Split the base64 string in data and contentType
        let block = stream.snap.split(";");
        let contentType = block[0].split(":")[1];
        let realData = block[1].split(",")[1];

        // Convert it to a blob to upload
        let blob = Base64Utils.b64toBlob(realData, contentType);
        let formDataToUpload = new FormData();

        formDataToUpload.append("image", blob);

        $.ajax({
            url: app_settings.api_url + "/saveSnapshot.php",
            data: formDataToUpload,
            type: "POST",
            contentType: false,
            processData: false,
            cache: false,
            dataType: "json",
            beforeSend: (xhr) => {
                xhr.setRequestHeader("gallery-id", Store.getCookie('gallery'));
                xhr.setRequestHeader("filter", stream.filter);
            },
            success: (data) => {
                // console.log('success', data);
            },
            error: (xhr: JQueryXHR, textStatus: string, errorThrown: string) => {
                // console.error('error', xhr);
            },
            complete: () => {
                // TODO: show notification (toast)
                this.streamChannel.send('saved', true);
            }
        });
    }


    /**
     * save snapshot by your browser (download folder)
     *
     * @param snap
     */
    private localSaveSnapshot(snap) {
        const timestamp = new Date().getTime();

        let $downloadImage = jQuery('<a/>', {
            id: timestamp,
            href: snap,
            download: timestamp + '.jpg'
        });

        $downloadImage[0].click();
    }
}