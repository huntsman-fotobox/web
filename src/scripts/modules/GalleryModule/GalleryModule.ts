///<reference path='../../VT3000/typings/references.ts' />

class GalleryModule extends BaseModule {

    protected galleryChannel: PubSub;
    protected previewSnapshot: any = null;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.galleryChannel = PubSub.join('Gallery');
        this.previewSnapshot = Route.getUrlParam('img');

        this.registerPubSubHandler();
        this.getTemplate("view.html", {}).then((t: JQuery) => {
            this.config.container.append(t);
            this.bind();

            let initGalleryId = Store.getCookie('gallery');
            this.fetchGallery(initGalleryId).then((images) => {
                this.galleryChannel.send('images', images);
            });

            deferred.resolve(true);
        });

        if (Assert.isNotUndefinedNullOrEmpty(this.previewSnapshot)) {
            this.fetchSnapshot(this.previewSnapshot).then((resp) => {
                this.galleryChannel.send('single', resp);
            });
        }

        return deferred.promise;
    }


    /**
     *
     */
    private registerPubSubHandler() {
        this.galleryChannel.on('gallery', (resp) => {
            this.fetchGallery(resp.id).then((images) => {
                this.galleryChannel.send('images', images);
            });
        });

        this.galleryChannel.on('remove', (resp) => {
            this.removeImage(resp.id).then(() => {
                this.galleryChannel.send('removed', {
                    id: resp.id
                });
            });
        });
    }


    /**
     *
     */
    protected fetchGallery(galleryId?): Q.Promise<any> {
        const deferred = Q.defer<any>();

        let paramData: any = {};
        if (galleryId) {
            paramData['gallery'] = galleryId;
        }

        this.api.get('/getImages.php', null, paramData).then((resp) => {
            deferred.resolve(resp);
        });

        return deferred.promise;
    }


    /**
     *
     */
    protected fetchSnapshot(snapshotId): Q.Promise<any> {
        const deferred = Q.defer<any>();

        this.api.get('/getImages.php', null, {id: snapshotId}).then((resp) => {
            deferred.resolve(resp);
        });

        return deferred.promise;
    }


    /**
     *
     * @param imageId
     */
    protected removeImage(imageId): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        let requestOptions: any = {
            'beforeSend': (xhr) => {
                xhr.setRequestHeader("image-id", imageId);
            }
        };

        this.api.post('/removeImages.php', {}, requestOptions).then((resp) => {
            deferred.resolve(true);
        });
        return deferred.promise;
    }
}