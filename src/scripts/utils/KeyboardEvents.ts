///<reference path='../VT3000/typings/references.ts' />

class KeyboardEvents {

    /**
     *
     * @param elem
     * @param key
     * @param cb
     *
     */
    public static onKeyUp(elem: JQuery = jQuery(document), key: any, cb: any) {
        let attr: string = (Assert.isNumber(key)) ? 'keyCode' : 'key';

        elem.on('keyup',(e) => {
            if (e[attr] === key) {
                return cb(e);
            }
        });
    };


    /**
     *
     * @param elem
     * @param key
     * @param cb
     *
     */
    public static onKeyDown(elem: JQuery = jQuery(document), key: any, cb: any) {
        let attr: string = (Assert.isNumber(key)) ? 'keyCode' : 'key';

        elem.on('keydown', (e) => {
            if (e[attr] === key) {
                return cb(e);
            }
        });
    };
}