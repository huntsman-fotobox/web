///<reference path='../VT3000/typings/references.ts' />

class TemplateFilters {

    public registerFilters(env: any): void {

        env.addFilter('imageSrc', (image) => {
            return app_settings.base_url_image + '/' + image['folderName'] + image['fileName'];
        });

        env.addFilter('baseUrl', (src) => {
            return app_settings.base_url + src;
        });

        env.addFilter('apiUrl', (src) => {
            return app_settings.api_url + src;
        });

        env.addFilter('formatBytes', (bytes, decimals) => {
            if (bytes) return '0 Bytes';
            let k = 1024,
                dm = decimals <= 0 ? 0 : decimals || 2,
                sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
                i = Math.floor(Math.log(bytes) / Math.log(k));

            return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
        });

        env.addFilter('shorten', (str, count) => {
            return str.slice(0, count || 5);
        });
    }
}