///<reference path='../VT3000/typings/references.ts' />

class Browser {

    /**
     * https://stackoverflow.com/questions/9847580/how-to-detect-safari-chrome-ie-firefox-and-opera-browser/9851769
     */
    public getType() {
        let browserType: string = '';

        // Opera 8.0+
        // @ts-ignore
        const isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        // @ts-ignore
        const isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]"
        /*
        const isSafari = false;
        function hund(obj) {
            return obj.toString() === "[object SafariRemoteNotification]";
        }

        let isHund = false;
        if(window["safari"] && typeof safari !== 'undefined') {
            isHund = hund(safari.pushNotification);
        }
        */

        // Internet Explorer 6-11
        // @ts-ignore
        const isIE = /*@cc_on!@*/ !!document.documentMode;

        // Edge 20+
        // @ts-ignore
        const isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1+
        // @ts-ignore
        const isChrome = !!window.chrome && !!window.chrome.webstore;

        // Blink engine detection
        // @ts-ignore
        const isBlink = (isChrome || isOpera) && !!window.CSS;


        switch (true) {
            case isOpera:
                browserType = 'opera';
                break;

            case isFirefox:
                browserType = 'firefox';
                break;

            case isIE:
                browserType = 'ie';
                break;

            case isEdge:
                browserType = 'ie -edge';
                break;

            case isChrome:
                browserType = 'chrome';
                break;

            case isBlink:
                browserType = 'blink';
                break;

            default:
                browserType = 'unknown-browser';
        }

        return browserType;
    }
}
