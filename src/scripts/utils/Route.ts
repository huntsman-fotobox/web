///<reference path='../VT3000/typings/references.ts' />

class Route {

    /**
     * get Url Parameter
     *
     * @param name
     */
    public static getUrlParam(name) {
        let results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (Assert.isUndefinedNullOrEmpty(results)) {
            return null;
        }
        else {
            return decodeURI(results[1]);
        }
    }


    /**
     * route to internal page
     *
     * @param path
     * @param options
     */
    public static to(path, options?: any) {
        let param: string = '';

        if (Assert.isObject(options)) {
            param += '?';
            for (let key in options) {
                param += encodeURIComponent(key) + '=' + encodeURIComponent(options[key]) + '&';
            }
        }

        return app_settings['base_url'] + path + '.html' + param.slice(0, -1);
    }


    /**
     * route to api page
     *
     * @param path
     */
    public static toApi(path) {
        return app_settings['api_url'] + path;
    }


    /**
     *
     * @param path
     */
    public static is(path) {
        return window.location.pathname === path + '.html';
    }
}