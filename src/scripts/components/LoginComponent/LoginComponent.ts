///<reference path='../../VT3000/typings/references.ts' />

class LoginComponent extends BaseComponent {

    protected notificationChannel: PubSub;
    protected loginChannel: PubSub;
    protected streamChannel: PubSub;

    protected currentGallery: any = {
        id: 'undefined',
        label: 'undefined'
    };

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.notificationChannel = PubSub.join('#Notification');
        this.loginChannel = PubSub.join('#Login');
        this.streamChannel = PubSub.join('#Stream');

        this.fetchGalleries().then((galleries) => {
            this.getTemplate("view.html", {galleries: galleries}).then((t: JQuery) => {
                this.config.container.html(t);
                this.bind();
                this.registerEventHandler();

                deferred.resolve(true);
            });
        });

        return deferred.promise;
    }


    /**
     * event handler
     */
    private registerEventHandler() {
        this.config.container.on('keyup', 'input', (e) => {
            let $elem = jQuery(e.currentTarget);

            this.currentGallery = {
                label: $elem.val()
            };
        });

        this.config.container.on('change', 'select', (e) => {
            let $elem = jQuery(e.currentTarget);
            let options = e.currentTarget.options;
            let $option = jQuery(options[options.selectedIndex]);

            this.currentGallery = {
                id: $option.attr('data-id'),
                label: $elem.val()
            };

            this.config.container.find('input').val(this.currentGallery.label);
        });


        this.config.container.on('click', 'button', (e) => {
            this.toggleStreamState();
        });

        KeyboardEvents.onKeyUp(jQuery(document), 'q', () => {
            this.toggleStreamState();
        });
    }


    /**
     *
     */
    private toggleStreamState() {
        let $btn = this.config.container.find('button');
        let streamState = $btn.attr('data-state');

        if (streamState === 'off') {
            $btn.attr('data-state', 'on');
            this.hideOverlay();
            this.setGallery();
        } else {
            $btn.attr('data-state', 'off');
            this.showOverlay();
            this.streamChannel.send('video', 'stop');
        }
    }


    /**
     * (create new gallery and)
     * set cookie
     * start stream
     */
    protected setGallery() {
        if (Assert.isUndefinedNullOrEmpty(this.currentGallery.id)) {
            this.saveGallery().then((gallery) => {
                this.startStream(gallery.id);
            });
        } else {
            this.startStream(this.currentGallery.id);
        }
    }


    /**
     *
     */
    private hideOverlay() {
        this.config.container.children().removeClass('-in');
    }


    /**
     *
     */
    private showOverlay() {
        this.config.container.children().addClass('-in');
    }


    /**
     *
     * @param id
     */
    private startStream(id) {
        Store.setCookie('gallery', id);
        this.streamChannel.send('video', 'play');
    }


    /**
     *
     */
    protected fetchGalleries(): Q.Promise<any> {
        const deferred = Q.defer<any>();

        this.api.get('/getGalleries.php').then((galleries) => {
            deferred.resolve(galleries);
        });

        return deferred.promise;
    }


    /**
     *
     */
    protected saveGallery(): Q.Promise<any> {
        const deferred = Q.defer<any>();

        let requestOptions: any = {
            'beforeSend': (xhr) => {
                xhr.setRequestHeader("gallery-label", this.currentGallery.label);
                xhr.setRequestHeader("gallery-slug", 'test');
            }
        };

        this.api.post('/saveGallery.php', {}, requestOptions).then((gallery) => {
            deferred.resolve(gallery);
        });

        return deferred.promise;
    }
}