///<reference path='../../../VT3000/typings/references.ts' />

import timeout = Q.timeout;

class SnapshotPreviewComponent extends BaseComponent {

    protected streamChannel: PubSub;
    protected notificationChannel: PubSub;

    protected $preview: JQuery;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.streamChannel = PubSub.join('#Stream');
        this.notificationChannel = PubSub.join('#Notification');

        this.getTemplate("view.html", {}).then((t: JQuery) => {
            this.config.container.append(t);
            this.bind();

            this.$preview = this.config.container.find('img');
            this.registerEventHandler();

            deferred.resolve(true);
        });

        return deferred.promise;
    }


    /**
     * event handler
     * > start / stop video
     * > take picture
     */
    private registerEventHandler() {
        this.streamChannel.on('snapshot', (resp) => {
            this.$preview.attr('src', resp.snap);
            this.showSnapshot();
        });
    }


    /**
     *
     */
    private showSnapshot() {
        this.streamChannel.send('view', 'open');
        this.config.container.children().addClass('-in');

        setTimeout(() => {
            this.hideSnapshot();
        }, 5000);
    }


    /**
     *
     */
    private hideSnapshot() {
        this.streamChannel.send('view', 'close');
        this.config.container.children().removeClass('-in');
    }
}