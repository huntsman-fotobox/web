///<reference path='../../../VT3000/typings/references.ts' />

class SnapshotThumbnailsComponent extends BaseComponent {

    protected streamChannel: PubSub;
    protected notificationChannel: PubSub;

    protected amountThumbnails: number = 8;
    protected currentGalleryId = null;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.streamChannel = PubSub.join('#Stream');
        this.notificationChannel = PubSub.join('#Notification');
        this.registerPubSubHandler();

        this.streamChannel.on('video', (state) => {
            if (state == 'play') {
                this.currentGalleryId = Store.getCookie('gallery');

                this.fetchImages().then((gallery) => {
                    this.renderThumbnails(gallery);
                });
            }
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     *
     * @param gallery
     */
    private renderThumbnails(gallery) {
        this.resetThumbnailList();

        this.getTemplate("view.html", gallery).then((t: JQuery) => {
            this.config.container.html(t);
            this.bind();
        });
    }


    /**
     * event handler
     */
    private registerPubSubHandler() {
        this.streamChannel.on('keyup', () => {
            this.hideGallery();
        });

        this.streamChannel.on('view', (resp) => {
            if (resp === 'close') {
                this.fetchImages().then((gallery) => {
                    this.renderThumbnails(gallery);
                    this.showGallery();
                });
            }
        });
    }


    /**
     *
     */
    private fetchImages(): Q.Promise<any> {
        const deferred = Q.defer<any>();

        this.api.get('/getImages.php', null, {
            limit: this.amountThumbnails,
            gallery: this.currentGalleryId
        }).then((resp) => {
            deferred.resolve(resp);
        });

        return deferred.promise;
    }


    /**
     *
     */
    private resetThumbnailList() {
        this.config.container.html('');
    }


    /**
     *
     */
    private hideGallery() {
        this.config.container.children().removeClass('-in');
    }


    /**
     *
     */
    private showGallery() {
        this.config.container.children().addClass('-in');
    }
}