///<reference path='../../../VT3000/typings/references.ts' />

class GalleryTopbarComponent extends BaseComponent {

    protected galleryChannel: PubSub;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.galleryChannel = PubSub.join('Gallery');

        this.fetchGalleries().then((galleries) => {
            this.getTemplate("view.html", {galleries: galleries}).then((t: JQuery) => {
                this.config.container.html(t);

                this.registerEventHandler();
            });
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * event handler
     */
    private registerEventHandler() {
        this.config.container.on('change', 'select', (e) => {
            let options = e.currentTarget.options;
            let galleryId = jQuery(options[options.selectedIndex]).attr('data-id');

            this.galleryChannel.send('gallery', {
                id: galleryId
            });
        });
    }


    /**
     *
     */
    protected fetchGalleries(): Q.Promise<any> {
        const deferred = Q.defer<any>();

        this.api.get('/getGalleries.php').then((galleries) => {
            deferred.resolve(galleries);
        });

        return deferred.promise;
    }
}