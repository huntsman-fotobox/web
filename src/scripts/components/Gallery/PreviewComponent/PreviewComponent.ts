///<reference path='../../../VT3000/typings/references.ts' />

class GalleryPreviewComponent extends BaseComponent {

    protected streamChannel: PubSub;
    protected notificationChannel: PubSub;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.streamChannel = PubSub.join('#Stream');
        this.notificationChannel = PubSub.join('#Notification');

        this.streamChannel.on('gallery-preview', (resp) => {

            this.fetchImages().then((galleryData) => {
                this.appendGallery(galleryData);
            });

        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * event handler
     * > start / stop video
     * > take picture
     */
    private registerEventHandler() {

    }


    /**
     *
     */
    private fetchImages(): Q.Promise<any> {
        const deferred = Q.defer<any>();

        this.api.get('/getImages.php', null, {
            gallery: Store.getCookie('gallery')
        }).then((resp) => {
            deferred.resolve(resp);
        });

        return deferred.promise;
    }


    /**
     *
     * @param gallery
     */
    private appendGallery(gallery) {
        this.getTemplate("view.html", gallery).then((t: JQuery) => {
            this.config.container.append(t);
            this.bind();

            const $lightbox = this.config.container.find('[data-lightbox]');
            const $gallery = this.config.container.find('[data-gallery]');
            const $images = this.config.container.find('[data-image]');

            let galleryWidth = $gallery[0].offsetWidth;
            let imageWidth = $images[0].offsetWidth;

            this.showLightbox();
            $lightbox.focus();

            KeyboardEvents.onKeyUp($lightbox, 'g', (e) => {
                this.streamChannel.send('video', 'play');
                this.hideLightbox();
            });

            KeyboardEvents.onKeyUp($lightbox, 32, (e) => {
                /*
                TODO
                let currentPos = $gallery[0].offsetLeft;
                let newPos = currentPos + imageWidth;


                $gallery.css('left', (newPos) * (-1));
                */
            });

            KeyboardEvents.onKeyUp($lightbox, 70, (e) => {
                // slide 'left'
            });
        });
    }


    /**
     *
     */
    private showLightbox() {
        this.config.container.children().addClass('-in');
    }

    /**
     *
     */
    private hideLightbox() {
        this.config.container.children().removeClass('-in');

        setTimeout(() => {
            this.config.container.html('');
        }, 1000);
    }
}