///<reference path='../../../VT3000/typings/references.ts' />

class ImageDetailviewComponent extends BaseComponent {

    protected galleryChannel: PubSub;
    protected lightboxChannel: PubSub;

    protected $content: any;
    protected currentImage: any;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.galleryChannel = PubSub.join('Gallery');
        this.lightboxChannel = PubSub.join('Lightbox');

        this.galleryChannel.on('single', (single) => {
            this.currentImage = single;

            this.renderImage(single).then(() => {

            });
        });

        this.getTemplate("view.html", {}).then((t: JQuery) => {
            this.config.container.html(t);

            this.$content = this.config.container.find('[data-content]');

            this.registerEventHandler();
            this.registerPubSubHandler();
        });

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * event handler
     */
    private registerEventHandler() {
        this.config.container.on('click', '[data-click="remove"]', (e) => {
            e.preventDefault();

            this.galleryChannel.send('remove', {id: this.currentImage.id});
        });
    }


    /**
     *
     */
    private registerPubSubHandler() {
        this.galleryChannel.on('removed', (resp) => {
            this.$content.html('');
        });
    }


    /**
     *
     */
    private renderImage(snapshot): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.getTemplate("content.html", {image: snapshot}).then((t: JQuery) => {
            this.$content.html(t);
            deferred.resolve(true);
        });

        return deferred.promise;
    }
}