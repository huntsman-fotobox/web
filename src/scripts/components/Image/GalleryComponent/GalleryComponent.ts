///<reference path='../../../VT3000/typings/references.ts' />

class ImageGalleryComponent extends BaseComponent {

    protected galleryChannel: PubSub;
    protected notificationChannel: PubSub;

    protected currentGallery: any = {};

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.galleryChannel = PubSub.join('Gallery');
        this.notificationChannel = PubSub.join('Notification');

        this.galleryChannel.on('images', (galleryData) => {
            this.currentGallery = galleryData;

            this.getTemplate("view.html", galleryData).then((t: JQuery) => {
                this.config.container.html(t);
            });
        });

        this.registerEventHandler();
        this.registerPubSubHandler();

        deferred.resolve(true);
        return deferred.promise;
    }


    /**
     * event handler
     */
    private registerEventHandler() {
        this.config.container.on('click', '[data-click="showDetails"]', (e) => {
            e.preventDefault();

            const $elem = jQuery(e.currentTarget).parents('[data-id]');
            let index = $elem.attr('data-index');

            this.galleryChannel.send('single', this.currentGallery.photos[index]);
        });
    }


    /**
     *
     */
    private registerPubSubHandler() {
        this.galleryChannel.on('next', (currentImage) => {

        });


        this.galleryChannel.on('prev', (currentImage) => {

        });


        this.galleryChannel.on('removed', (resp) => {
            this.config.container.find('[data-id="' + resp.id + '"]').remove();
        });
    }
}