///<reference path='../../VT3000/typings/references.ts' />

class CameraStreamComponent extends BaseComponent {

    protected streamChannel: PubSub;
    protected notificationChannel: PubSub;

    protected cameraStreamIsOnline: boolean = false;
    protected snapshotInProgress: boolean = false;
    protected $video: any;
    protected stream: any = new MediaSource();

    protected filterSize: number = 5;
    protected currentFilter: number = 0;

    protected $keyUpArea: JQuery;

    public init(): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();

        this.streamChannel = PubSub.join('#Stream');
        this.notificationChannel = PubSub.join('#Notification');

        this.getTemplate("view.html", {}).then((t: JQuery) => {
            this.config.container.append(t);
            this.bind();

            this.$keyUpArea = this.config.container.find('[tabindex][data-keyup="camera-stream"]');
            this.$video = document.getElementById('camera-stream');
            this.initStream();
            this.registerPubSubEvents();
            this.registerEventListener();

            deferred.resolve(true);
        });

        return deferred.promise;
    }


    /**
     * https://jsfiddle.net/dannymarkov/cuumwch5/
     *
     * https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia
     * https://kittmedia.com/2013/nginx-fehler-413-request-entity-too-large/
     */
    private initStream() {
        if (!navigator.mediaDevices) {
            this.notificationChannel.send('error', {msg: "Your browser doesn't have support for the navigator.getUserMedia interface."});
        } else {
            let videoOptions: any = {
                video: {
                    width: 800,
                    height: 600
                }
            };

            navigator.mediaDevices.getUserMedia(videoOptions)
                .then((stream) => {
                    this.stream = stream;
                })
                .catch((err) => {
                    this.notificationChannel.send('error', {
                        msg: "There was an error with accessing the camera stream: " + err.name,
                        error: err
                    });
                });
        }
    }


    /**
     * pubsub event handler
     * > start / stop video
     * > wait for preview closed
     */
    private registerPubSubEvents() {
        this.streamChannel.on('video', (resp) => {
            switch (resp) {
                case 'play':
                    this.playVideoStream();
                    break;
                case 'stop':
                    this.stopVideoStream();
                    break;
            }
        });

        this.streamChannel.on('view', (response) => {
            if (response === 'close') {
                this.snapshotInProgress = false;
            }
        });
    }


    /**
     * event handler
     * > take a snapshot
     * > change filter
     */
    private registerEventListener() {
        KeyboardEvents.onKeyUp(this.$keyUpArea, 32, () => {
            if (this.cameraStreamIsOnline && !this.snapshotInProgress) {
                this.streamChannel.send('keyup', true);
                this.snapshotInProgress = true;

                this.countdown().then(() => {
                    this.streamChannel.send('snapshot', {
                        snap: this.getSnapshot(),
                        filter: this.config.container.find('video').attr('data-filter') || 0
                    });
                });
            }
        });


        KeyboardEvents.onKeyUp(this.$keyUpArea, 70, (e) => {
            this.setFilter();
        });


        KeyboardEvents.onKeyUp(this.$keyUpArea, 'g', (e) => {
            this.streamChannel.send('gallery-preview', true);
            this.stopVideoStream();
        });
    }


    /**
     * start video stream
     */
    private playVideoStream() {
        try {
            this.$video.srcObject = this.stream;
        } catch (error) {
            this.$video.src = URL.createObjectURL(this.stream);
        }

        this.config.container.find('video').addClass('-in');
        this.$video.play();
        this.$keyUpArea.focus();
        this.cameraStreamIsOnline = true;
    }


    /**
     * stop video stream
     */
    private stopVideoStream() {
        this.config.container.find('video').removeClass('-in');
        this.$video.pause();
        this.$video.src = "";
        this.cameraStreamIsOnline = false;

        // this.stream.getTracks()[0].stop();
    }


    /**
     *
     */
    private countdown(countdown = 3): Q.Promise<boolean> {
        const deferred = Q.defer<boolean>();
        const $elem = this.config.container.find('#countdown');

        $elem.html('');

        let x = setInterval(() => {
            let $item = jQuery('<div />');
            $item.text(countdown);
            $elem.append($item);

            countdown--;

            if (countdown <= 0) {
                clearInterval(x);

                setTimeout(() => {
                    deferred.resolve(true);
                }, 2000);
            }
        }, 1100);

        return deferred.promise;
    }


    /**
     *
     */
    private getSnapshot() {
        const $canvas = this.config.container.find('canvas')[0];
        const context = $canvas.getContext('2d', {
            alpha: false
        });
        const width = this.$video.videoWidth;
        const height = this.$video.videoHeight;

        if (width && height) {

            // Setup a canvas with the same dimensions as the video.
            $canvas.width = width;
            $canvas.height = height;

            // set filter
            context.filter = this.getFilter();

            // flip camera stream
            context.translate(width, 0);
            context.scale(-1, 1);

            // Make a copy of the current frame in the video on the canvas.
            context.drawImage(this.$video, 0, 0, width, height);

            // Turn the canvas image into a dataURL that can be used as a src for our photo.
            return $canvas.toDataURL('image/png');
        }
    }


    /**
     *  return css filter
     */
    private getFilter() {
        return this.config.container.find('video').css('filter');
    }


    /**
     *
     */
    private setFilter() {
        let $video = this.config.container.find('video');

        this.currentFilter++;
        if (this.currentFilter <= this.filterSize) {
            $video.attr('data-filter', this.currentFilter);
        } else {
            this.currentFilter = 0;
            $video.attr('data-filter', 0);
        }
    }
}