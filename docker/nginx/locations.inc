location / {
  root   /usr/share/nginx/html;
  index  index.html index.htm;
}

location /patternlab {
    root   /usr/share/nginx;
    index  index.html index.htm;
}

location /sassdoc {
    root   /usr/share/nginx;
    index  index.html index.htm;
}

location /typedoc {
    root   /usr/share/nginx;
    index  index.html index.htm;
}