var fs = require('fs');
var YAML = require('node-yaml');
var currentEnvFilePath = './environment.yml';
var envExamplePath = './tool/data/example_environment.yml';
var fileExists = fs.existsSync(currentEnvFilePath, 'utf8');

if (!fileExists){
  console.info("\x1b[42m", "INFO: environment.yml does not exist, copying from example", '\x1b[0m');
  fs.copyFileSync(envExamplePath, currentEnvFilePath);
  return;
} else{
  var exampleEnvFile = fs.readFileSync(envExamplePath);
  var currentEnvFile = fs.readFileSync(currentEnvFilePath);
  var exampleEnv = YAML.parse(exampleEnvFile);
  var currentEnv = YAML.parse(currentEnvFile);
  var updateCurrentEnv = false;

  for (var newProperty in exampleEnv){
    if (!currentEnv.hasOwnProperty(newProperty)){
      var defaultValue = exampleEnv[newProperty];
      console.info('\x1b[36m', 'INFO: Current environment.yml is missing (new) property "' + newProperty + '", adding default value "' + defaultValue + '" to local environment.yml', '\x1b[0m');
      currentEnv[newProperty] = defaultValue;
      updateCurrentEnv = true;
    }
  }

  for (var oldProperty in currentEnv){
    if (!exampleEnv.hasOwnProperty(oldProperty)){
      console.warn('\x1b[31m', 'WARNING: Current environment.yml is missing (new) property "' + oldProperty + '", removing value from environment.yml', '\x1b[0m');
      delete currentEnv[oldProperty];
      updateCurrentEnv = true;
    }
  }

  if (updateCurrentEnv){
    fs.writeFileSync(currentEnvFilePath, YAML.dump(currentEnv, null, 2));
  }

}
