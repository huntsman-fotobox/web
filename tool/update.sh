#!/usr/bin/env bash
php="/Users/ETNAII/Projects/bs/huntsman-fotobox/php"
web="/Users/ETNAII/Projects/bs/huntsman-fotobox/web"

cd $php
./build.sh

cd $web
./build.sh


green=`tput setaf 2`
reset=`tput sgr0`

echo "${green}huntsman is up to date!${reset}"