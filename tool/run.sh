#!/usr/bin/env bash
php="/Users/ETNAII/Projects/bs/huntsman-fotobox/php"
web="/Users/ETNAII/Projects/bs/huntsman-fotobox/web"

cd $php
./start.sh

cd $web
./start.sh

docker ps

/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --app=http://localhost:8080/

green=`tput setaf 2`
reset=`tput sgr0`

echo "${green}huntsman is running!${reset}"